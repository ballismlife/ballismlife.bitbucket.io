"use strict";

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

/* ^^^
 * Глобальные-вспомогательные функции
 * ========================================================================== */

/* ^^^
 * Viewport Height Correction
 *
 * @link https://www.npmjs.com/package/postcss-viewport-height-correction
 * ========================================================================== */
function setViewportProperty() {
  var vh = window.innerHeight * 0.01;
  document.documentElement.style.setProperty('--vh', vh + 'px');
}

window.addEventListener('resize', setViewportProperty);
setViewportProperty(); // Call the fuction for initialisation

/* ^^^
 * Возвращает HTML-код иконки из SVG-спрайта
 *
 * @param {String} name Название иконки из спрайта
 * @param {Object} opts Объект настроек для SVG-иконки
 *
 * @example SVG-иконка
 * getSVGSpriteIcon('some-icon', {
 *   tag: 'div',
 *   type: 'icons', // colored для подключения иконки из цветного спрайта
 *   class: '', // дополнительные классы для иконки
 *   mode: 'inline', // external для подключаемых спрайтов
 *   url: '', // путь до файла спрайта, необходим только для подключаемых спрайтов
 * });
 */

function getSVGSpriteIcon(name, opts) {
  opts = _extends({
    tag: 'div',
    type: 'icons',
    "class": '',
    mode: 'inline',
    url: ''
  }, opts);
  var external = '';
  var typeClass = '';

  if (opts.mode === 'external') {
    external = "".concat(opts.url, "/sprite.").concat(opts.type, ".svg");
  }

  if (opts.type !== 'icons') {
    typeClass = " svg-icon--".concat(opts.type);
  }

  opts["class"] = opts["class"] ? " ".concat(opts["class"]) : '';
  return "\n    <".concat(opts.tag, " class=\"svg-icon svg-icon--").concat(name).concat(typeClass).concat(opts["class"], "\" aria-hidden=\"true\" focusable=\"false\">\n      <svg class=\"svg-icon__link\">\n        <use xlink:href=\"").concat(external, "#").concat(name, "\"></use>\n      </svg>\n    </").concat(opts.tag, ">\n  ");
}
/* ^^^
 * JQUERY Actions
 * ========================================================================== */


$(function () {
  'use strict';
  /**
   * определение существования элемента на странице
   */

  $.exists = function (selector) {
    return $(selector).length > 0;
  };

  $(".about-news.owl-carousel").owlCarousel({
    items: 3,
    loop: true,
    margin: 0,
    dots: false,
    nav: true,
    responsive: {
      0: {
        items: 1,
        autoWidth: false,
        autoHeight: true
      },
      481: {
        items: 2,
        autoWidth: false,
        autoHeight: true
      },
      1026: {
        items: 3,
        autoWidth: false,
        autoHeight: true
      }
    }
  });
  $(".js-btn-burger").on("click", function (e) {
    e.preventDefault();
    $(this).toggleClass("btn-burger-is-active");

    if ($(this).hasClass("btn-burger-is-active")) {
      $(this).html('<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><path fill="#369" fill-rule="evenodd" d="M8.586 10L.808 17.778l1.414 1.414L10 11.414l7.778 7.778 1.414-1.414L11.414 10l7.778-7.778L17.778.808 10 8.586 2.222.808.808 2.222 8.586 10z"></path></svg>');
    } else {
      $(this).html('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="16" viewBox="0 0 24 16"><path fill="#369" fill-rule="evenodd" d="M0 0h24v2H0V0zm0 7h24v2H0V7zm0 7h24v2H0v-2z"></path></svg>');
    }

    $("body").toggleClass("hat_content_open");
    $(".b-mobile-panel .top-menu--mobile").toggleClass("menu-is-active");
    $(".b-mobile-panel__overlay-menu").toggleClass("overlay-is-active");
  });
  $(".b-mobile-panel__overlay-menu").on("click", function () {
    $(this).toggleClass("overlay-is-active");
    $("body").toggleClass("hat_content_open");
    $(".b-mobile-panel .top-menu--mobile").toggleClass("menu-is-active");
  });
  $(".js-calendar-items.owl-carousel").owlCarousel({
    items: 5,
    loop: true,
    margin: 0,
    dots: false,
    nav: true,
    responsive: {
      0: {
        items: 1,
        autoWidth: false,
        autoHeight: true
      },
      375: {
        items: 2,
        autoWidth: false,
        autoHeight: true
      },
      481: {
        items: 3,
        autoWidth: false,
        autoHeight: true
      },
      769: {
        items: 4,
        autoWidth: false,
        autoHeight: true
      },
      1367: {
        items: 5,
        autoWidth: false,
        autoHeight: true
      }
    }
  });
  $(".languages__link").each(function () {
    $(this).on("click", function (e) {
      e.preventDefault();
      $(this).addClass("active").siblings().removeClass("active");
    });
  });
  var PAGE = $('html, body');
  var pageScroller = $('.page-scroller');
  var inMemoryClass = 'page-scroller--memorized';
  var isVisibleClass = 'page-scroller--visible';
  var enabledOffset = 60;
  var pageYOffset = 0;
  var inMemory = false;

  function resetPageScroller() {
    setTimeout(function () {
      if (window.pageYOffset > enabledOffset) {
        pageScroller.addClass(isVisibleClass);
      } else if (!pageScroller.hasClass(inMemoryClass)) {
        pageScroller.removeClass(isVisibleClass);
      }
    }, 150);

    if (!inMemory) {
      pageYOffset = 0;
      pageScroller.removeClass(inMemoryClass);
    }

    inMemory = false;
  }

  if (pageScroller.length > 0) {
    window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
      passive: true
    } : false);
    pageScroller.on('click', function (event) {
      event.preventDefault();
      window.removeEventListener('scroll', resetPageScroller);

      if (window.pageYOffset > 0 && pageYOffset === 0) {
        inMemory = true;
        pageYOffset = window.pageYOffset;
        pageScroller.addClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: 0
        }, 500, 'swing', function () {
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      } else {
        pageScroller.removeClass(inMemoryClass);
        PAGE.stop().animate({
          scrollTop: pageYOffset
        }, 500, 'swing', function () {
          pageYOffset = 0;
          window.addEventListener('scroll', resetPageScroller, window.supportsPassive ? {
            passive: true
          } : false);
        });
      }
    });
  }
});