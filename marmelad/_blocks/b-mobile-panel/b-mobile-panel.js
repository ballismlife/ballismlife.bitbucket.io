$(".js-btn-burger").on("click", function(e) {
  e.preventDefault();
  $(this).toggleClass("btn-burger-is-active");

  if ($(this).hasClass("btn-burger-is-active")) {
    $(this).html(
      '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"><path fill="#369" fill-rule="evenodd" d="M8.586 10L.808 17.778l1.414 1.414L10 11.414l7.778 7.778 1.414-1.414L11.414 10l7.778-7.778L17.778.808 10 8.586 2.222.808.808 2.222 8.586 10z"></path></svg>'
    );
  } else {
    $(this).html(
      '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="16" viewBox="0 0 24 16"><path fill="#369" fill-rule="evenodd" d="M0 0h24v2H0V0zm0 7h24v2H0V7zm0 7h24v2H0v-2z"></path></svg>'
    );
  }

  $("body").toggleClass("hat_content_open");
  $(".b-mobile-panel .top-menu--mobile").toggleClass("menu-is-active");
  $(".b-mobile-panel__overlay-menu").toggleClass("overlay-is-active");
});

$(".b-mobile-panel__overlay-menu").on("click", function() {
  $(this).toggleClass("overlay-is-active");
  $("body").toggleClass("hat_content_open");
  $(".b-mobile-panel .top-menu--mobile").toggleClass("menu-is-active");
});
