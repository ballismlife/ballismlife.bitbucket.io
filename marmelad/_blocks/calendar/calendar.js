$(".js-calendar-items.owl-carousel").owlCarousel({
  items: 5,
  loop: true,
  margin: 0,
  dots: false,
  nav: true,
  responsive: {
    0: {
      items: 1,
      autoWidth: false,
      autoHeight: true
    },
    375: {
      items: 2,
      autoWidth: false,
      autoHeight: true
    },
    481: {
      items: 3,
      autoWidth: false,
      autoHeight: true
    },
    769: {
      items: 4,
      autoWidth: false,
      autoHeight: true
    },
    1367: {
      items: 5,
      autoWidth: false,
      autoHeight: true
    }
  }
});
