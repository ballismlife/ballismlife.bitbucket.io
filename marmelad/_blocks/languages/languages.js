$(".languages__link").each(function() {
  $(this).on("click", function(e) {
    e.preventDefault();
    $(this)
      .addClass("active")
      .siblings()
      .removeClass("active");
  });
});
