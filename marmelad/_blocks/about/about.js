$(".about-news.owl-carousel").owlCarousel({
  items: 3,
  loop: true,
  margin: 0,
  dots: false,
  nav: true,
  responsive: {
    0: {
      items: 1,
      autoWidth: false,
      autoHeight: true
    },
    481: {
      items: 2,
      autoWidth: false,
      autoHeight: true
    },
    1026: {
      items: 3,
      autoWidth: false,
      autoHeight: true
    }
  }
});
